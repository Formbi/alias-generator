#!/bin/sh
exec guile -l "$0" -- "$0" "$@"
!#

(use-modules (ice-9 rdelim)
	     (ice-9 popen)
	     (ice-9 string-fun)
	     (ice-9 getopt-long)
	     (ice-9 pretty-print))

(define HOME (getenv "HOME"))

(define PATH
  (let [(PATH1
	 (string-replace-substring
	  (getenv "PATH")
	  (string-append ":" HOME "/.local/share/alias-generator/bin")
	  ""))
	(PATH2
	 (string-replace-substring
	  (getenv "PATH")
	  (string-append HOME "/.local/share/alias-generator/bin:")
	  ""))]
    PATH2))

(define alias-dir
  (string-append HOME "/.local/share/alias-generator/bin/"))

(define $* " $* ")

(define scheme-mode-info "
;; Local Variables:
;; mode: scheme
;; End:
")

(define println
  (case-lambda
    ((obj)      
     (display obj) (newline))
    ((obj port) 
     (display obj port) (newline port))))

(define (command-output command)
  (let* ((port (open-input-pipe command))
	 (str  (read-delimited "\0" port)))
    (close-pipe port)
    (string-trim-right str)))

(define (which- program)
  (command-output (string-append "PATH=" PATH " which " program)))

(define (which program)
  (string-append " " (which- program) " "))

(define sh-shebang
  (format #f "#!~A
"
	  (which- "bash")))

(define scm-shebang
  (format #f "#!~A
exec ~A -l $0 -- $0 $@
!#
"
	(which- "bash") (which- "guile")))


(define (alias-script str)
  (string-append sh-shebang
		 str "\n"))

(define (alias name . strs)
  (call-with-output-file (string-append alias-dir name)
    (lambda (port)
      (println (string-trim-both (apply string-append strs)))
      (println sh-shebang port)
      (display (string-trim-both (apply string-append strs)) port)))
  (system (format #f "chmod +x '~a'" (string-append alias-dir name))))

(define (scm-alias name . sexps)
  (call-with-output-file (string-append alias-dir name)
    (lambda (port)
      (map (lambda (sexp)
	     (pretty-print sexp)
	     (newline))
	   sexps)
      (println scm-shebang port)
      (map (lambda (sexp)
	     (pretty-print sexp port)
	     (newline port))
	   sexps)
      (println "(exit)" port)
      (display scheme-mode-info port)))
  (system (format #f "chmod +x '~a'" (string-append alias-dir name))))


(unless (or
	 (member "-r" (command-line))
	 (member "--repl" (command-line)))
  (begin
    (load (string-append HOME "/.config/aliases.scm"))
    (exit)))
